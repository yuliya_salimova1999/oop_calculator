﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OOP_Calculator
{
    class Calculator
    {
        private ICalculate calculator;
        private List<KeyValuePair<string, BinaryOperation>> operationsLog = new List<KeyValuePair<string, BinaryOperation>>();
        private double? previousValue = null;
        private Matrix previousMatrix = null;
        private bool toUsePreviousValue = false;
        public void Sum()
        {
            IValidate validator = new DoubleTypeInputValidator();
            if (toUsePreviousValue && previousValue != null)
            {
                calculator = new SumOperation((double)previousValue, (double)validator.Validate());
            }
            else
            {
                calculator = new SumOperation((double)validator.Validate(), (double)validator.Validate());
            }
            BinaryOperation operation = (BinaryOperation)calculator.Calculate();
            previousValue = (double)operation.Result;
            operationsLog.Add(new KeyValuePair<string, BinaryOperation>("Sum", operation));

        }
        public void Substract()
        {
            IValidate validator = new DoubleTypeInputValidator();
            if (toUsePreviousValue && previousValue != null)
            {
                calculator = new substractOperation((double)previousValue, (double)validator.Validate());
            }
            else
            {
                calculator = new substractOperation((double)validator.Validate(), (double)validator.Validate());
            }
            BinaryOperation operation = (BinaryOperation)calculator.Calculate();
            previousValue = (double)operation.Result;
            operationsLog.Add(new KeyValuePair<string, BinaryOperation>("Substract", operation));
        }
        public void Multiply()
        {
            IValidate validator = new DoubleTypeInputValidator();
            if (toUsePreviousValue && previousValue != null)
            {
                calculator = new NumbersMultiplicationOperation((double)previousValue, (double)validator.Validate());
            }
            else
            {
                calculator = new NumbersMultiplicationOperation((double)validator.Validate(), (double)validator.Validate());
            }
            BinaryOperation operation = (BinaryOperation)calculator.Calculate();
            previousValue = (double)operation.Result;
            operationsLog.Add(new KeyValuePair<string, BinaryOperation>("Multiply", operation));
        }
        public void Devide()
        {
            IValidate validator = new DoubleTypeInputValidator();
            if (toUsePreviousValue && previousValue != null)
            {
                calculator = new DevideOperation((double)previousValue, (double)validator.Validate());
            }
            else
            {
                calculator = new DevideOperation((double)validator.Validate(), (double)validator.Validate());
            }
            BinaryOperation operation = (BinaryOperation)calculator.Calculate();
            previousValue = (double)operation.Result;
            operationsLog.Add(new KeyValuePair<string, BinaryOperation>("Devide", operation));
        }
        public void MultiplyMatrixes()
        {
            int rowsA = 0;
            int columnsA = 0;
            IValidate validator = new MatrixRowsAndColumnsInputValidator();
            if (!toUsePreviousValue || previousMatrix == null)
            {
                Console.WriteLine("Enter number of rows(r) and columns(col) of the first matrix:");
                Console.WriteLine("(r)");
                rowsA = (int)validator.Validate();
                Console.WriteLine("(col)");
                columnsA = (int)validator.Validate();
            }
            Console.WriteLine("Enter number of rows(r) and columns(col) of the second matrix:");
            Console.WriteLine("(r)");
            int rowsB = (int)validator.Validate();
            Console.WriteLine("(col)");
            int columnsB = (int)validator.Validate();
            if (toUsePreviousValue && previousMatrix != null && previousMatrix.Columns == rowsB)
            {
                calculator = new MatrixesMultiplicationOperation(previousMatrix, FillTheMatrix(rowsB, columnsB));
            }
            else if (columnsA == rowsB)
            {
                calculator = new MatrixesMultiplicationOperation(FillTheMatrix(rowsA, columnsA), FillTheMatrix(rowsB, columnsB));
            }
            else
            {
                Console.WriteLine("Multiplication of matrixes can't be executed!");
            }
            BinaryOperation operation = (BinaryOperation)calculator.Calculate();
            previousMatrix = (Matrix)operation.Result;
            operationsLog.Add(new KeyValuePair<string, BinaryOperation>("Matrixes multiplication", operation));
        }
        public void SwitchPreviousValueUsageMode()
        {
            if (toUsePreviousValue)
            {
                toUsePreviousValue = false;
                Console.WriteLine("PREVIOUSLY SAVED VALUE USE MODE IS DIACTIVATED!\n");
            }
            else
            {
                toUsePreviousValue = true;
                Console.WriteLine("PREVIOUSLY SAVED VALUE USE MODE IS ACTIVATED!\n");
            }
        }
        public void ShowOperationsLog()
        {
            if (operationsLog.Any())
            {
                Console.WriteLine("\n---------------------------");
                foreach (KeyValuePair<string,BinaryOperation> oper in operationsLog)
                {
                    Console.WriteLine(oper.Key);
                }
                Console.WriteLine("---------------------------\n");
            }
            else
            {
                Console.WriteLine("No operations have been executed yet!");
            }
        }
        private Matrix FillTheMatrix(int rows, int columns)
        {
            IValidate validator = new DoubleTypeInputValidator();
            double[,] matrix = new double[rows, columns];
            Console.WriteLine("Fill the matrix r = " + rows + ", col = " + columns);
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < columns; col++)
                {
                    Console.WriteLine("Enter elem"  + (row + 1) + (col + 1) + ":");
                    matrix[row, col] = (double)validator.Validate();
                }

            }
            return new Matrix(matrix);
        }
    }
}
