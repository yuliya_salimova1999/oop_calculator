﻿using System;

namespace OOP_Calculator
{
    class DoubleTypeInputValidator : IValidate
    {
        object IValidate.Validate()
        {
            string input = Console.ReadLine();
            double argument;
            bool justABool = true;
            while (justABool)
            {
                if ((!input.Contains(" ")) && double.TryParse(input, out argument))
                {
                    return argument;
                }
                Console.WriteLine("Wrong input! Try again: ");
                input = Console.ReadLine();

            }
            return null;
        }
    }
}