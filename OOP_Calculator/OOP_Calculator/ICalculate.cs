﻿namespace OOP_Calculator
{
    public interface ICalculate
    {
        object Calculate();
    }
}