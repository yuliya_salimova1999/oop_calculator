﻿using System;

namespace OOP_Calculator
{
    class DevideOperation : BinaryOperation, ICalculate
    {
        public DevideOperation(double theFirstArgument, double theSecondArgument)
        {
            this.theFirstArgument = theFirstArgument;
            this.theSecondArgument = theSecondArgument;
        }
        object ICalculate.Calculate()
        {
            result = (double)theFirstArgument / (double)theSecondArgument;
            Console.WriteLine(theFirstArgument + " / " + theSecondArgument + " = " + result);
            return this;
        }
    }
}