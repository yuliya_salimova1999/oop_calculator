﻿using System;

namespace OOP_Calculator
{
    class MatrixesMultiplicationOperation : BinaryOperation, ICalculate
    {
        public MatrixesMultiplicationOperation(Matrix theFirstArgument, Matrix theSecondArgument)
        {
            this.theFirstArgument = theFirstArgument;
            this.theSecondArgument = theSecondArgument;
        }
        object ICalculate.Calculate()
        {
            MultiplyMatrixes();
            PrintMatrix((Matrix)theFirstArgument, "A");
            PrintMatrix((Matrix)theSecondArgument, "B");
            PrintMatrix((Matrix)result, "A * B = C");
            return this;
        }
        private void MultiplyMatrixes()
        {
            int rowsA = ((Matrix)theFirstArgument).Rows;
            int columnsA = ((Matrix)theFirstArgument).Columns;
            int columnsB = ((Matrix)theSecondArgument).Columns;
            double[,] resultMatrix = new double[rowsA, columnsB];
            for (int rowA = 0; rowA < rowsA; rowA++)
            {
                for (int colB = 0; colB < columnsB; colB++)
                {
                    for (int colA = 0; colA < columnsA; colA++)
                    {
                        resultMatrix[rowA, colB] += ((Matrix)theFirstArgument).MatrixArray[rowA, colA] * ((Matrix)theSecondArgument).MatrixArray[colA, colB];
                    }
                }
            }
            result = new Matrix(resultMatrix);
        }
        private void PrintMatrix(Matrix matrix, string matrixName)
        {
            Console.WriteLine("Matrix " + matrixName);
            Console.WriteLine("\n---------------------------");
            for (int row = 0; row < matrix.Rows; row++)
            {
                for (int col = 0; col < matrix.Columns; col++)
                {
                    if (col == matrix.Columns - 1)
                    {
                        Console.Write(matrix.MatrixArray[row, col] + "\n");
                    }
                    else
                    {
                        Console.Write(matrix.MatrixArray[row, col] + " ");
                    }
                }
            }
            Console.WriteLine("---------------------------\n");
        }
    }
}