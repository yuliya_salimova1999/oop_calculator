﻿using System;

namespace OOP_Calculator
{
    class MatrixRowsAndColumnsInputValidator : IValidate
    {
        object IValidate.Validate()
        {
            string input = Console.ReadLine();
            int argument;
            bool justABool = true;
            while (justABool)
            {
                if ((!input.Contains(" ")) && int.TryParse(input, out argument) && argument > 0)
                {
                    return argument;
                }
                else
                {
                    Console.WriteLine("Wrong input! Try again: ");
                    input = Console.ReadLine();
                }
            }
            return null;
        }
    }
}