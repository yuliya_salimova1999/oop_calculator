﻿using System;

namespace OOP_Calculator
{
    class substractOperation : BinaryOperation, ICalculate
    {
        public substractOperation(double theFirstArgument, double theSecondArgument)
        {
            this.theFirstArgument = theFirstArgument;
            this.theSecondArgument = theSecondArgument;
        }
        object ICalculate.Calculate()
        {
            result = (double)theFirstArgument - (double)theSecondArgument;
            Console.WriteLine(theFirstArgument + " - " + theSecondArgument + " = " + result);
            return this;
        }
    }
}