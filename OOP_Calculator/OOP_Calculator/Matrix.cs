﻿namespace OOP_Calculator
{
    class Matrix
    {
        private double[,] matrix = null;
        private int columns = 0;
        private int rows = 0;

        public Matrix(double[,] matrix)
        {
            this.matrix = matrix;
            rows = matrix.GetUpperBound(0) + 1;
            columns = matrix.Length/rows;
        }
        public int Rows => rows;
        public int Columns => columns;
        public double[,] MatrixArray => matrix;
    }
}