﻿using System;

namespace OOP_Calculator
{
    class UI
    {
        static void Main(string[] args)
        {
            Calculator calculator = new Calculator();
            bool shellState = true;
            while (shellState)
            {
                Console.WriteLine("1. Sum\n2. Substract\n3. Multiply\n4. Devide\n5. Multiply matrixes\n" +
                                "6. Use/Not use previosly saved value\n7. Show operations log\n8. Clear\n9. Exit\n");
                Console.WriteLine("Choose an operation: ");
                string symbol = Console.ReadLine();
                switch (symbol)
                {
                    case "1":
                        calculator.Sum();
                        break;
                    case "2":
                        calculator.Substract();
                        break;
                    case "3":
                        calculator.Multiply();
                        break;
                    case "4":
                        calculator.Devide();
                        break;
                    case "5":
                        calculator.MultiplyMatrixes();
                        break;
                    case "6":
                        calculator.SwitchPreviousValueUsageMode();
                        break;
                    case "7":
                        calculator.ShowOperationsLog();
                        break;
                    case "8":
                        Console.Clear();
                        break;
                    case "9":
                        Environment.Exit(0);
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}