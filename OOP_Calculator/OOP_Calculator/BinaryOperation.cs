﻿namespace OOP_Calculator
{
    abstract class BinaryOperation
    {
        protected object theFirstArgument = null;
        protected object theSecondArgument = null;
        protected object result = null;

        public object TheFirstArgument => theFirstArgument;
        public object TheSecondArgument => theSecondArgument;
        public object Result => result;
    }
}