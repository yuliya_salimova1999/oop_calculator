﻿using System;

namespace OOP_Calculator
{
    class NumbersMultiplicationOperation : BinaryOperation, ICalculate
    {
        public NumbersMultiplicationOperation(object theFirstArgument, object theSecondArgument)
        {
            this.theFirstArgument = theFirstArgument;
            this.theSecondArgument = theSecondArgument;
        }
        object ICalculate.Calculate()
        {
            result = (double)theFirstArgument * (double)theSecondArgument;
            Console.WriteLine(theFirstArgument + " * " + theSecondArgument + " = " + result);
            return this;
        }
    }
}